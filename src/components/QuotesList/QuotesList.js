import React, {Component} from 'react'
import Quote from "./Quote/Quote";
import './QuotesList.css'
import axios from 'axios'
import Spinner from "../UI/Spinner/Spinner";

class QuotesList extends Component {
	state = {
		quotesList: [],
		loading: false,
		category: null
	};
	
	updateQuotes = () => {
		this.setState({loading: true});
		let url = '/quotesList.json';
		let category = null;
		if (this.props.match.params.category) {
			category = this.props.match.params.category;
			url = `/quotesList.json?orderBy="category"&equalTo="${category}"`;
		}
		axios.get(url).then(response => {
			this.setState({loading: false});
			let quotesList = [];
			for (let key in response.data) {
				quotesList.push({
					id: key,
					author: response.data[key].author,
					text: response.data[key].text,
					category: response.data[key].category
				});
			}
			this.setState({quotesList});
		});
	};
	
	deleteQuoteHandler = (id) => {
		const quotesList = [...this.state.quotesList];
		const index = quotesList.findIndex(p => p.id === id);
		this.setState({loading: true});
		axios.delete(`/quotesList/${id}.json`).finally(() => {
			quotesList.splice(index, 1);
			this.setState({quotesList, loading: false});
		});
	};
	
	editQuoteHandler = (id) => {
		const quotesList = [...this.state.quotesList];
		const index = quotesList.findIndex(p => p.id === id);
		const quote = quotesList[index];
		this.props.history.push({
			pathname: '/quotes/' + quote.id + '/edit'
		})
	};
	
	componentDidMount() {
		this.updateQuotes();
	}
	
	componentDidUpdate() {
		let category = this.props.match.params.category;
		if (category !== this.state.category) {
			this.setState({category});
			this.updateQuotes();
		}
	}
	
	render() {
		let QuotesList = (
			<div className="QuotesList">
				<div className="QuotesListTitle">{this.props.title}</div>
				<ul>
					{
						this.state.quotesList.map(quote => {
							return <Quote
								key={quote.id}
								author={quote.author}
								text={quote.text}
								delete={() => this.deleteQuoteHandler(quote.id)}
								edit={() => this.editQuoteHandler(quote.id)}
							/>;
						})
					}
				</ul>
			</div>
		);
		if (this.state.loading) QuotesList = <Spinner/>;
		return QuotesList;
	}
}

export default QuotesList;