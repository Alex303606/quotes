import React from 'react'
import './Quote.css'
const Quote = props => {
	return (
		<li className="Quote">
			<div className="Quote__author">{props.author}</div>
			<div className="QuoteText">{props.text}</div>
			<div className="Quote__buttons">
				<button onClick={props.delete} className="Delete">Delete</button>
				<button onClick={props.edit} className="Edit">Edit</button>
			</div>
		</li>
	)
};

export default Quote;