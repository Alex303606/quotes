import React,{Component} from 'react'
import {NavLink} from "react-router-dom";
import './Header.css'
class Header extends Component {
	render(){
		return(
			<header>
				<div className="logo">Quotes central</div>
				<ul>
					<li><NavLink exact to="/quotes">Quotes</NavLink></li>
					<li><NavLink to="/add-quote">Submit new quote</NavLink></li>
				</ul>
			</header>
		)
	}
}

export default Header;