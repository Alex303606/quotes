import React, {Component} from 'react'
import axios from "axios/index";
import {NavLink} from "react-router-dom";
import './Sidebar.css'

class Sidebar extends Component {
	state = {
		categoriesList: []
	};
	
	async componentDidMount() {
		this.setState({loading: true});
		let categories = await axios.get('/categories.json').finally(() => {
			this.setState({loading: false});
		});
		let categoriesList = [];
		for (let key in categories.data) {
			categoriesList.push({name: key, value: categories.data[key]});
		}
		this.setState({categoriesList});
	};
	
	render() {
		return (
			<ul className="Sidebar">
				<li><NavLink to="/quotes">All</NavLink></li>
				{
					this.state.categoriesList.map(category => {
						return <li key={category.value}>
							<NavLink
								exact
								to={'/quotes/' +  category.value}>
								{category.name}
							</NavLink>
						</li>
					})
				}
			</ul>
		)
	}
}

export default Sidebar;