import React, {Component, Fragment} from 'react';
import Header from "./components/Header/Header";
import {Route, Switch} from "react-router-dom";
import Submit from "./containers/Submit/Submit";
import Quotes from "./containers/Quotes/Quotes";

class App extends Component {
	render() {
		return (
			<Fragment>
				<Header/>
				<Switch>
					<Route exact path="/" component={Quotes}/>
					<Route exact path="/quotes" component={Quotes}/>
					<Route exact path="/quotes/:id/edit" component={Submit}/>
					<Route path="/quotes/:category" component={Quotes}/>
					<Route exact path="/add-quote" component={Submit}/>
				</Switch>
			</Fragment>
			
		);
	}
}

export default App;
