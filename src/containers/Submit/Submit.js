import React, {Component} from 'react'
import './Submit.css'
import axios from 'axios'
import Spinner from "../../components/UI/Spinner/Spinner";

class Submit extends Component {
	state = {
		quote: {
			author: '',
			text: '',
			category: ''
		},
		title: 'Submit new quote',
		categoriesList: [],
		loading: false
	};
	
	async componentDidMount() {
		this.setState({loading: true});
		let quote = {...this.state.quote};
		let categories = await axios.get('/categories.json').finally(() => {
			this.setState({loading: false});
		});
		let categoriesList = [];
		for (let key in categories.data) {
			categoriesList.push({name: key,value: categories.data[key]});
		}
		quote.category = categoriesList[0].value;
		console.log(categoriesList);
		this.setState({categoriesList, quote});
		if (this.props.match.params.id) {
			const id = this.props.match.params.id;
			this.setState({loading: true});
			let quote = await axios.get(`/quotesList/${id}.json`).finally(() => {
				this.setState({loading: false});
			});
			this.setState({quote: quote.data,title: 'Edit quote'});
		}
	};
	
	changeTextHandler = (event) => {
		let quote = {...this.state.quote};
		quote.text = event.target.value;
		this.setState({quote});
	};
	
	changeAuthorHandler = (event) => {
		let quote = {...this.state.quote};
		quote.author = event.target.value;
		this.setState({quote});
	};
	
	changeCategoryHandler = (event) => {
		let quote = {...this.state.quote};
		quote.category = event.target.value;
		this.setState({quote});
	};
	
	saveQuoteHandler = (e) => {
		e.preventDefault();
		let quote = {...this.state.quote};
		if (this.props.match.params.id){
			axios.put(`/quotesList/${this.props.match.params.id}.json`, quote).finally(() => {
				this.props.history.replace('/');
			});
		} else {
			axios.post('/quotesList.json', quote).finally(() => {
				this.props.history.replace('/');
			});
		}
	};
	
	render() {
		console.log(this.state.quote.category);
		let submit = (
			<div className="Submit">
				<h2>{this.state.title}</h2>
				<form onSubmit={(e) => this.saveQuoteHandler(e)}>
					<div className="row">
						<label htmlFor="category">Category</label>
						<select value={this.state.quote.category}
						        onChange={(event) => this.changeCategoryHandler(event)} id="category">
							{
								this.state.categoriesList.map(category => {
									return <option key={category.value} value={category.value}>{category.name}</option>
								})
							}
						</select>
					</div>
					<div className="row">
						<label htmlFor="author">Author</label>
						<input required value={this.state.quote.author}
						       onChange={(event) => this.changeAuthorHandler(event)} type="text" id="author"/>
					</div>
					<div className="row">
						<label htmlFor="text">Quote text</label>
						<textarea required value={this.state.quote.text}
						          onChange={(event) => this.changeTextHandler(event)} rows="10"/>
					</div>
					<button className="Submit__save">Save</button>
				</form>
			</div>
		);
		if (this.state.loading) submit = <Spinner/>;
		return submit;
	}
}

export default Submit;

