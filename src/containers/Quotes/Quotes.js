import React,{Component} from 'react'
import './Quotes.css'
import Sidebar from "../../components/Sidebar/Sidebar";
import QuotesList from "../../components/QuotesList/QuotesList";
class Quotes extends Component {
	render(){
		return (
			<div className="Quotes">
				<Sidebar/>
				<QuotesList {...this.props}/>
			</div>
		)
	}
}

export default Quotes;